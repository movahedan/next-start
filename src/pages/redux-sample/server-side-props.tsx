import { useRef } from "react";

import { reduxWrapper, useStoreDispatch, useStoreSelector } from "lib/store";
import { actions, selectors } from "lib/store/slices";

import type { NextPage } from "next";

const ReduxSamplePage: NextPage = () => {
  const countSetterRef = useRef<HTMLInputElement>(null);

  const dispatch = useStoreDispatch();
  const count = useStoreSelector(selectors.sampleSlice.countSelector);

  const onSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();

    const { value } = countSetterRef.current || {};
    if (value) dispatch(actions.sampleSlice.setCount(Number(value)));
  };

  return (
    <div>
      <div>Sample slice: Clock!</div>
      <p>Everytime you hit this page it reset to 35! and continue ticking</p>
      <p>
        Tick: <span>{count}</span>
      </p>

      <input
        ref={countSetterRef}
        type="number"
        placeholder="Please enter a value"
      />

      <button onClick={onSubmit}>Submit</button>
    </div>
  );
};

export const getServerSideProps = reduxWrapper.getServerSideProps(
  (store) => async () => {
    console.log({
      state: selectors.sampleSlice.indexSelector(store.getState()),
    });

    store.dispatch(actions.sampleSlice.setCount(35));

    return Promise.resolve({ props: {} });
  }
);

export default ReduxSamplePage;
