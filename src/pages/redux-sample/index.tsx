import { useRef } from "react";

import { useStoreDispatch, useStoreSelector } from "lib/store";
import { actions, selectors } from "lib/store/slices";

import type { NextPage } from "next";

const ReduxSamplePage: NextPage = () => {
  const countSetterRef = useRef<HTMLInputElement>(null);

  const dispatch = useStoreDispatch();
  const sampleSlice = useStoreSelector(selectors.sampleSlice.indexSelector);
  const sampleSliceCount = useStoreSelector(
    selectors.sampleSlice.countSelector
  );

  const onSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();

    const { value } = countSetterRef.current || {};
    if (value) dispatch(actions.sampleSlice.setCount(Number(value)));
  };

  const onAsyncSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault();

    const { value } = countSetterRef.current || {};
    if (value) {
      await dispatch(actions.sampleSlice.fetchCount(Number(value)));
    }
  };

  return (
    <>
      <div className="page">
        <div>Sample slice: Clock!</div>
        <p>
          Tick: <span>{sampleSliceCount}</span>
        </p>
        <p>
          Loading: <span>{sampleSlice.loading ? "true" : "false"}</span>
        </p>

        <input
          ref={countSetterRef}
          type="number"
          placeholder="Please enter a value"
        />

        <button onClick={onSubmit}>Submit</button>

        <button onClick={onAsyncSubmit}>Async submit</button>
      </div>

      <style jsx>
        {`
          .page {
            @apply p-4 text-xl;

            input[type="number"] {
              @apply pl-2 mt-4 mr-4 bg-gray-200 rounded;
            }

            button {
              @apply px-4 mx-2 text-white bg-gray-900 rounded;
            }
          }
        `}
      </style>
    </>
  );
};

export default ReduxSamplePage;
