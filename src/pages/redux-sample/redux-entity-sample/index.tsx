import { useRef } from "react";

import {
  actions,
  selectors,
  useStoreDispatch,
  useStoreSelector,
} from "lib/store";

import type { NextPage } from "next";
import type { FC } from "react";

const ReduxSamplePage: NextPage = () => {
  const dispatch = useStoreDispatch();
  const sampleEntities = useStoreSelector(selectors.sampleEntity.listSelector);

  const onAddCounter = async (e: { preventDefault: () => void }) => {
    e.preventDefault();

    await dispatch(actions.sampleEntity.createCount());
  };

  return (
    <>
      <div className="page">
        <div>Sample entity slice: More Clocks!</div>

        <button onClick={onAddCounter}>Add a counter</button>

        <ul>
          {Object.values(sampleEntities.entities).map(
            (entity) => !!entity && <Counter key={entity.id} id={entity.id} />
          )}
        </ul>
      </div>

      <style jsx>
        {`
          .page {
            @apply w-1/2 p-4 text-xl;

            button {
              @apply px-4 my-4 text-white bg-gray-900 rounded;
            }
          }
        `}
      </style>
    </>
  );
};

export default ReduxSamplePage;

const Counter: FC<{ id: string }> = ({ id }) => {
  const countSetterRef = useRef<HTMLInputElement>(null);

  const dispatch = useStoreDispatch();
  const count = useStoreSelector(selectors.sampleEntity.itemCountSelector(id));

  const onSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();

    const { value } = countSetterRef.current || {};
    if (value) {
      dispatch(actions.sampleEntity.setCount({ id, count: Number(value) }));
    }
  };

  return (
    <>
      <div className="counter">
        <p>
          Tick: <span>{count}</span>
        </p>

        <input
          ref={countSetterRef}
          type="number"
          placeholder="Please enter a value"
        />

        <button onClick={onSubmit}>Submit</button>
      </div>

      <style jsx>
        {`
          .counter {
            @apply flex items-center w-auto p-4 mb-3 bg-gray-300;

            input {
              @apply pl-2 ml-auto bg-gray-100 rounded;
            }

            button {
              @apply px-4 mx-2 ml-4 text-white bg-gray-900 rounded;
            }
          }
        `}
      </style>
    </>
  );
};
