import { createSlice } from "@reduxjs/toolkit";

import { fetchCount } from "./actions";
import { reducers, extraReducers } from "./reducers";
import { sampleSliceInitialState as initialState } from "./state";

export const sampleSlice = createSlice({
  name: "sampleSlice",
  initialState,
  reducers,
  extraReducers,
});

export const sampleSliceActions = {
  ...sampleSlice.actions,
  fetchCount,
};
