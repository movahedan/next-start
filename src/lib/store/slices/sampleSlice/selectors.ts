import { ISampleSlice } from "./type";

export const indexSelector = (state: { sampleSlice: ISampleSlice }) =>
  state.sampleSlice;

export const countSelector = (state: { sampleSlice: ISampleSlice }) =>
  indexSelector(state).count;
