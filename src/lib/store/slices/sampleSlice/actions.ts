import { createAsyncThunk } from "@reduxjs/toolkit";

type fetchCountResponse = number;
type fetchCountRequestParams = number;

export const fetchCount = createAsyncThunk<
  fetchCountResponse,
  fetchCountRequestParams
>("sampleSlice/fetchCount", async (payload: fetchCountRequestParams) => {
  const response = new Promise<fetchCountResponse>((res) => {
    setTimeout(() => {
      res(payload);
    }, 350);
  });

  return (await response) as fetchCountResponse;
});
