import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";

import { fetchCount } from "./actions";
import { ISampleSlice } from "./type";

export const tick: CaseReducer<ISampleSlice> = (state) => {
  state.count++;
};

export const setCount: CaseReducer<ISampleSlice, PayloadAction<number>> = (
  state,
  action
) => {
  state.count = action.payload;
};

const fetchCountFulfilled: CaseReducer<ISampleSlice, PayloadAction<number>> = (
  state,
  action
) => {
  state.loading = false;
  state.count = action.payload;
};

export const reducers = { tick, setCount };

// Action reducers ------------------------------------------------------------

const fetchCountPending: CaseReducer<ISampleSlice> = (state) => {
  state.loading = true;
};

const fetchCountReducers = {
  [fetchCount.fulfilled.type]: fetchCountFulfilled,
  [fetchCount.pending.type]: fetchCountPending,
};

export const extraReducers = {
  ...fetchCountReducers,
};
