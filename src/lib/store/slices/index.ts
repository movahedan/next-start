import {
  sampleEntity,
  sampleEntityActions,
  sampleEntitySelectors,
} from "./sampleEntity";
import {
  sampleSlice,
  sampleSliceActions,
  sampleSliceSelectors,
} from "./sampleSlice";

import type { ISample } from "./sampleEntity";
import type { ISampleSlice } from "./sampleSlice";
import type { EntityState } from "@reduxjs/toolkit";

export type ISlices = {
  sampleSlice: ISampleSlice;
  sampleEntity: EntityState<ISample>;
};

export const reducers = {
  sampleSlice: sampleSlice.reducer,
  sampleEntity: sampleEntity.reducer,
};

export const actions = {
  sampleSlice: sampleSliceActions,
  sampleEntity: sampleEntityActions,
};

export const selectors = {
  sampleSlice: sampleSliceSelectors,
  sampleEntity: sampleEntitySelectors,
};
