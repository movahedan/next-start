import {
  EntityId,
  EntityState,
  CaseReducer,
  PayloadAction,
} from "@reduxjs/toolkit";

import { createCount } from "./actions";
import { sampleEntityAdapter } from "./adapters";
import { ISample } from "./type";

export const tick: CaseReducer<
  EntityState<ISample>,
  PayloadAction<{ id: EntityId }>
> = (state, action) => {
  const { id } = action.payload;
  const entitity = state.entities[id];

  if (entitity) {
    sampleEntityAdapter.updateOne(state, {
      id,
      changes: {
        count: entitity.count + 1,
      },
    });
  }
};

export const setCount: CaseReducer<
  EntityState<ISample>,
  PayloadAction<{ id: EntityId; count: number }>
> = (state, action) => {
  const { id, count } = action.payload;
  const entitity = state.entities[id];

  if (entitity) {
    sampleEntityAdapter.updateOne(state, {
      id,
      changes: {
        count,
      },
    });
  }
};

export const reducers = { tick, setCount };

// Action reducers ------------------------------------------------------------
const fetchCountReducers = {
  [createCount.fulfilled.type]: sampleEntityAdapter.addOne,
};

export const extraReducers = {
  ...fetchCountReducers,
};
