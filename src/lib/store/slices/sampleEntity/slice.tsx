import { createSlice } from "@reduxjs/toolkit";

import { fetchCount, createCount } from "./actions";
import { reducers, extraReducers } from "./reducers";
import { sampleEntityInitialState as initialState } from "./state";

export const sampleEntity = createSlice({
  name: "sampleEntity",
  initialState,
  reducers,
  extraReducers,
});

export const sampleEntityActions = {
  ...sampleEntity.actions,
  fetchCount,
  createCount,
};
