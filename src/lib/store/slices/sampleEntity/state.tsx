import { EntityState } from "@reduxjs/toolkit";

import { sampleEntityAdapter } from "./adapters";
import { ISample } from "./type";

export const sampleEntityInitialState: EntityState<ISample> =
  sampleEntityAdapter.getInitialState();
