import { createAsyncThunk, nanoid } from "@reduxjs/toolkit";

export type fetchCountResponse = { id: string; count: number };
type fetchCountRequestParams = string;

export const fetchCount = createAsyncThunk(
  "articles/fetchCount",
  async (id: fetchCountRequestParams) => {
    const data = await new Promise<fetchCountResponse>((res) => {
      setTimeout(() => {
        res({ id, count: 5 });
      }, 350);
    });

    return data;
  }
);

export const createCount = createAsyncThunk(
  "articles/createCount",
  async () => {
    const data = await new Promise<fetchCountResponse>((res) => {
      setTimeout(() => {
        res({ id: nanoid(), count: 5 });
      }, 350);
    });

    return data;
  }
);
