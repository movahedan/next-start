import { EntityId, EntityState } from "@reduxjs/toolkit";

import { ISample } from "./type";

export const listSelector = (state: { sampleEntity: EntityState<ISample> }) =>
  state.sampleEntity;

export const itemSelector =
  (id: EntityId) => (state: { sampleEntity: EntityState<ISample> }) =>
    listSelector(state).entities[id];

export const itemCountSelector =
  (id: EntityId) => (state: { sampleEntity: EntityState<ISample> }) =>
    itemSelector(id)(state)?.count;
