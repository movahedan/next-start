import { configureStore } from "@reduxjs/toolkit";
import { createWrapper, HYDRATE } from "next-redux-wrapper";
import { useDispatch, useSelector } from "react-redux";
import { Store, combineReducers } from "redux";

import { reducers } from "./slices";

import type { ISlices } from "./slices";
import type { TypedUseSelectorHook } from "react-redux";

const store = configureStore<ISlices>({
  reducer: (state, action) => {
    if (action.type === HYDRATE) {
      const hydratedState: ISlices = {
        // TODO Implement apply delta
        ...state, // use previous state
        ...action.payload, // apply delta from hydration
      };

      return hydratedState;
    } else {
      const combineReducer = combineReducers<ISlices>(reducers);

      return combineReducer(state, action);
    }
  },
});

type AppDispatch = typeof store.dispatch;
type AppState = ReturnType<typeof store.getState>;

export const useStoreSelector: TypedUseSelectorHook<AppState> = useSelector;
export const useStoreDispatch = () => useDispatch<AppDispatch>();
export const reduxWrapper = createWrapper<Store<AppState>>(() => {
  // TODO store.dispatch({ type: HYDRATE, payload: slicesInitialStates });

  return store;
});

export * from "./slices";
