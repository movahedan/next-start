export const analyticsId = process.env.NEXT_PUBLIC_ANALYTICS_ID;
export const analyticsTrackingId =
  process.env.NEXT_PUBLIC_ANALYTICS_TRACKING_ID;
